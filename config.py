# implemented by: @b3ljTM

MAX_COINS = 50

# All coins listed in those lists will not be fteched
TO_EXCLUDE = ['BTC', 'ETH']
EXCAHNGE_COINS = ['BNB', 'KCS', 'CRO']
STABLE_COINS = ['USDT', 'BUSD', 'USDC', 'UST', 'DAI']
FORBIDDEN = ['C98', 'IDEX', 'SAFEMOON', 'BEL', 'DYDX', 'ALPACA',
             'BADGER', 'FOR', 'SUN', 'ALPHA', 'OM', 'BOND',
             'REEF', 'KAVA', 'BTCST', 'BURGER', 'BETA', 'TROY', 'REN',
             'EPS', 'MLN', 'FARM', 'GVT', 'FRON', 'DF', 'UMA', 'MIR',
             'RAY', 'SRM', 'RAMP', 'FIRE', 'PLOT', 'PERP', 'COMP', 'AKRO',
             'XVS', 'HEGIC', 'PERL', 'WIN', 'WBTC', 'SUSHI', 'MFT', 'DEXE',
             'SHIB', 'RUNE', 'CAKE', 'TRU', 'AAVE', 'UNI', 'VIB', 'PPT',
             'YFII', 'YFI', 'CREAM', 'GNO', 'MKR', 'REP', 'AUDIO', 'FUN', 'HARD', 'RCN']
