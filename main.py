# implemented by: @b3ljTM

from datetime import datetime
import requests
from bs4 import BeautifulSoup
from config import *

now = datetime.now()
filename = now.strftime("%d_%m_%Y_%H_%M_%S")
filename = filename + ".csv"
coins_counter = 0
page = 1
url = 'https://www.coingecko.com/en?page='
fp = open(str(filename), 'w')
#	Coin	Price	1h	24h	7d	24hVolume	MktCap
fp.write('Coin,Price,1h,24h,7d,24h Volume,Market Cap\n')


while coins_counter < MAX_COINS and page < 115:
    print(f"_____________Page: {page} _____________")
    content = requests.get(url+str(page), timeout=(3.05, 27))
    soup = BeautifulSoup(content.text, 'html.parser')
    for lines in soup.find_all('tr'):
        try:
            coin_symbol = lines.find("a", class_="d-lg-none font-bold")
            if coin_symbol.text.strip() in TO_EXCLUDE or coin_symbol.text.strip() in FORBIDDEN or coin_symbol.text.strip() in EXCAHNGE_COINS or coin_symbol.text.strip() in STABLE_COINS:
                continue
            else:
                coin_price = lines.find(
                    "td", class_="td-price price text-right")
                coin_1h = lines.find(
                    "td", class_="td-change1h change1h stat-percent text-right col-market")
                coin_24h = lines.find(
                    "td", class_="td-change24h change24h stat-percent text-right col-market")
                coin_7d = lines.find(
                    "td", class_="td-change7d change7d stat-percent text-right col-market")
                coin_24hVolume = lines.find(
                    "td", class_="td-liquidity_score lit text-right %> col-market")
                coin_marketCap = lines.find(
                    "td", class_="td-market_cap cap col-market cap-price text-right")
            #print (f'{coin_symbol.text.strip()},{coin_price.text.strip()},{coin_1h.text.strip()},{coin_24h.text.strip()},{coin_7d.text.strip()},{coin_24hVolume.text.replace(",",".").strip()},{coin_marketCap.text.replace(",",".").strip()}')
            fp.write(f'{coin_symbol.text.strip()},{coin_price.text.replace(",",".").strip()},{coin_1h.text.strip()},{coin_24h.text.strip()},{coin_7d.text.strip()},{coin_24hVolume.text.replace(",",".").strip()},{coin_marketCap.text.replace(",",".").strip()}\n')
            coins_counter += 1
        except Exception as e:
            print(e)
        if coins_counter >= MAX_COINS:
            break
    page += 1
fp.close()
