# coingecko crawler
 
**Coingecko Crawler** is Python script for web scraping data from the Coingecko website. The script uses the BeautifulSoup library to extract information such as cryptocurrency prices, market capitalizations, and trading volumes from Coingecko's API. The data is then stored in a local database or exported to a CSV file for further analysis. With Coingecko Crawler, users can easily gather up-to-date data on various cryptocurrencies and track their performance over time.

## Requirements installing

```bash
pip3 install -r requirements.txt
```

## Configuring 🧰

MAx number of coins to fetch
```python
MAX_COINS = 10
```

All coins listed in those lists will **NOT** be fteched
```python
TO_EXCLUDE = ['BTC', 'ETH']
EXCAHNGE_COINS = ['BNB', 'KCS', ]
STABLE_COINS = ['USDT', 'BUSD', 'USDC', 'UST', 'DAI']
FORBIDDEN = ['C98', 'IDEX', 'SAFEMOON', 'BEL', 'DYDX', 'ALPACA',
             'BADGER', 'FOR', 'SUN', 'ALPHA', 'OM', 'BOND',
             'REEF', 'KAVA', 'BTCST', 'BURGER', 'BETA', 'TROY', 'REN',
             'EPS', 'MLN', 'FARM', 'GVT', 'FRON', 'DF', 'UMA', 'MIR',
             'RAY', 'SRM', 'RAMP', 'FIRE', 'PLOT', 'PERP', 'COMP', 'AKRO',
             'XVS', 'HEGIC', 'PERL', 'WIN', 'WBTC', 'SUSHI', 'MFT', 'DEXE',
             'SHIB', 'RUNE', 'CAKE', 'TRU', 'AAVE', 'UNI', 'VIB', 'PPT',
             'YFII', 'YFI', 'CREAM', 'GNO', 'MKR', 'REP', 'AUDIO', 'FUN', 'HARD', 'RCN']
```

## Running 🏃
To run the Crawler 
```bash
python3 main.py
```

## Results

The output is a CSV file.

| Coin|Price|1h|24h|7d|24h Volume|Market Cap
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| SOL|$202.87|-0.3%|-5.6%|5.6%|$5.638.872.684|$61.967.444.980 |
| ADA|$1.42|-0.2%|-9.9%|-7.8%|$3.578.633.116|$45.448.132.663 |
| XRP|$0.840221|0.8%|-9.4%|-10.6%|$6.136.021.469|$39.507.660.797 |
| DOT|$29.37|-0.3%|-13.3%|-15.1%|$2.519.770.043|$31.187.564.707 |
| LUNA|$71.13|-2.0%|5.9%|74.0%|$4.976.376.695|$27.558.577.978 |
| DOGE|$0.177285|-0.4%|-11.7%|-12.4%|$3.786.993.813|$23.457.179.575 |
| AVAX|$91.99|-0.2%|-13.6%|-11.9%|$1.909.647.923|$22.298.872.198 |
| CRO|$0.593616|-1.5%|-6.4%|-13.7%|$785.905.403|$15.000.076.712 |
| MATIC|$1.96|-1.2%|-13.9%|19.6%|$3.902.339.143|$13.446.558.399 |
| LTC|$163.27|-0.6%|-13.7%|-16.4%|$3.316.632.002|$11.281.726.568 |

## LICENSE
GPL v3.0

## SUPPORT ME 🥰
```js
USDT (TRC20) : TZHisChRRBmUrBMxzBCK2LMScHknAb33Vx
BTC : 
ADA : 
```

